[Glitchtip](https://glitchtip.com) is an open source application monitoring and error tracking platform. Funkwhale uses Glitchtip to track and collate errors from Funkwhale pods. Pod admins can opt in to share this information with the collective or use their own server.

Glitchtip is compatible with [Sentry](https://sentry.io). Funkwhale implements the [Sentry SDK](https://docs.sentry.io) to send information from Funkwhale pods to Glitchtip.

## Configuration

You can configure the Sentry SDK using the following parameters in the `.env` file:

* `FUNKWHALE_SENTRY_DSN`: the endpoint to which Funkwhale sends error information. Add your Glitchtip URL here, or use the Funkwhale Collective's instance.
  * `FUNKWHALE_SENTRY_DSN=https://5840197379c64f65aad3c5c09274994d@am.funkwhale.audio/1`
* `FUNKWHALE_SENTRY_SR`: configures the sample rate for error tracking. This value defaults to `0.25`, meaning the SDK tracks **1/4** errors. To report all errors, change this to `1`.

## Sentry properties

Funkwhale sets the following properties in the Sentry SDK:

* [`dsn`](https://docs.sentry.io/platforms/python/configuration/options/#dsn): the data source name. Configured by `FUNKWHALE_SENTRY_DSN` in the `.env` file
* [`debug`](https://docs.sentry.io/platforms/python/configuration/options/#debug): whether the SDK runs in debug mode. Configured by `DEBUG` in the `.env` file
* [`release`](https://docs.sentry.io/platforms/python/configuration/options/#release): the current Funkwhale Version
* [`environment`](https://docs.sentry.io/platforms/python/configuration/options/#environment): Set to "api" for the backend and "prod" or "develop" for the frontend

## Custom tags

Funkwhale adds the following custom tags to error payloads:

- `instance`: the base URL of the reporting Funkwhale pod