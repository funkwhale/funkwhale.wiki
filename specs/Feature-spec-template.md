> Use this template to create a specification for API endpoints and new features. [name=Sporiff]

## The issue

> Describe the issue you want to resolve or gap in the software you want to fill. [name=Sporiff]

## Proposed solution

> Briefly put forward a proposed solution. Outline some key behaviors and explain how this solution addresses the issue laid out above. [name=Sporiff]

## Feature behavior

> Describe the behavior of the feature. Lay out where the feature will be included as well as its back- and front-end behavior. [name=Sporiff]

### Backend

> Define the behavior of the feature from the **backend** perspective [name=Sporiff]

### Frontend

> Define the behavior of the feature from the **frontend** perspective [name=Sporiff]

## Availability

> Where is this going to be available to end users? [name=Sporiff]

- [ ] Admin panel
- [ ] App frontend
- [ ] CLI

## Responsible parties

> Describe which working groups need to be involved in the project and what their responsibilities are. [name=Sporiff]

## Open questions

> Note any open questions that need to be addressed by the responsible parties. [name=Sporiff]

## Minimum viable product

> Describe what the simplest acceptable version of this feature looks like. It's important to release features for testing even if they don't have everything you've described, so try to break it down into smaller, shippable features. [name=Sporiff]

### Next steps

> Note down the next steps **after** the MVP. Decide what needs to be addressed and in what order. [name=Sporiff]
