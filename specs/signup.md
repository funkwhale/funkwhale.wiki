![Status:ready](https://badgen.net/badge/Status/Ready/green)

# Signup flow

## The issue

When users sign up to a Funkwhale instance, the UI needs to display clear and meaningful messages regarding their account status. These messages differ based on how the admin sets up the authorization flow on the pod.

## Behavior

The signup behavior flow is summarized in the flowchart below:

```mermaid
flowchart TD
    pod[User chooses a pod] --> open{Are registrations open?}
    open --> |no| pod
    open --> |yes| signup[The user signs\nup on the pod]
    signup --> enforce{Is email verification\nenforced?}
    enforce --> |yes| verify[The user verifies their email]
    enforce --> |no| verification{Is manual verification\nrequired?}
    verify --> verification
    verification --> |yes| authorize[The pod admin\nauthorizes the signup]
    authorize --> signin[The user signs in to the pod]
    verification --> |no| signin
```

During the above process, there are two major bottlenecks where users must be shown *meaningful* messages.

### Email verification

If email verification is **enforced**, the user must verify the email address they sign up with. Until they do this, they can't log in.

If a user attempts to log in while their email address is **unverified**, the API should send a [`401 Unauthorized`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/401) status code with a message indicating that the email isn't verified. The frontend should display a message that the user needs to check their email. This message should also provide the user the ability to *resend* the verification email and a contact address for the admin.

### Manual verification required

If the pod admin has enabled **Manual sign-up verification**, they must process all requests activate user accounts.

If a user tries to sign in **before** their request has been processed by a pod admin, the API should send a [`401 Unauthorized`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/401) status code with a message indicating that the account is not verified. The frontend should display a message informing the user that the pod admin needs to verify the signup request. This message should also provide the user with a contact address for the admin.

### Admin flow

If the pod admin enables **both** manual sign-up verification **and** email verification, they should not receive the verification request until the user verifies their email address. This reduces the number of illegitimate account verifications they need to handle. This also reduces confusion on the side of the user as they receive failure messages that describe where they are in the signup process.

```mermaid
flowchart TD
    setup[The pod admin enables\nemail verification and\nsign-up verification] --> signup[A user signs up\nto the pod] 
    signup --> email[The user is told to verify\ntheir email address] 
    email --> verify[The user verifies\ntheir email address] 
    verify --> request[A request is sent\nto the admin to verify\nthe user's account] 
    request --> choose{The admin determines whether\nto verify the account}
    choose --> |Confirm| confirm[The user receives a confirmation]
    choose --> |Reject| reject[The admin rejects\nthe account creation]
    confirm --> signin[The user signs in]
```
