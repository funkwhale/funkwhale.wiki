## The issue

Allow multiples artists per tracks and album in the background and front of fw. Main issur is about how to show artist credit in the front has it appears in the track or release (how to get join phrases and aliases)

## Proposed solution

Add backend attributes to save the joind phrase and artist credit querying the musicbrainz db.

Associated forum post : https://forum.funkwhale.audio/d/247-multi-artist-albums-and-tracks/36

## Feature behavior


### Backend

1.1. Generate a new `artists` entry in the models

1.2. Query mb api during import to get the correct join phrase and generate the `artists` content
endpoints :
https://musicbrainz.org/ws/2/release/3cfa5ceb-9e55-4ece-b2a8-635c67f78166?inc=artists
https://musicbrainz.org/ws/2/recording/fad14dab-5c9f-4934-be42-18c17372350d?inc=artists

1.3 : artists fw attribute will be a list of dictionaries :`` [{"artist_fw_url" : $artist_fw_url, "artist credit" : $artist credit, "join phrase" : $join phrase}, {"artist_fw_url2" : $artist_fw_url2, "artist2 credit" : $artist2 credit, "join phrase" : $join phrase}]``.

1.4 Create a migration command. 


### Frontend

2.1. Use the `artists.artist_credit` tag as artist ui name for albums and tracks
2.2. Create a special page for the special Various Artist artist.
2.3 Create an also appears in section with a list of albums where the artist appears (in the artist page)


### Documentation 
1. Document migration

## Availability

> Where is this going to be available to end users? [name=Sporiff]

- [ ] Admin panel
- [x] App frontend
- [ ] CLI

## Responsible parties

Backend dev group need to implement the feature first. Then Frontend dev group can implement the features. We already agreed how the data will be store but if there is any change the forum post is still open for discussion.

## Open questions

## Minimum viable product

Album and tracks with multiples artist :
- are correctly credited according to the mb db in the UI.
- are associated to each artist in the db.

### Next steps

1. Create a task that updates the artists fiels on `python manage.py import_files` quering the mb dd and on file reupload.
2. Handle multiple spelling per artist for single artist track and albums
