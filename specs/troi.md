# Music recommendation investigation

## The issue

The listening experience in Funkwhale is currently based entirely around user-defined radios, playlists, and queues. Discovering new music in Funkwhale is limited, which presents the following issues:

1. Discoverability for artists on Funkwhale is limited and difficult to achieve
2. Automatic generation of playlists, available on platforms such as Spotify and Deezer, isn't available on Funkwhale

We would like to investigate a way to recommend content to users to address these issues.

## Proposed solution

There are two possible paths to resolve this issue:

1. Funkwhale designs its own recommendation algorithm
2. Funkwhale integrates an existing recommendation library into its system

Option 1 isn't workable as it puts a lot of strain on the project for something that may not be used by everyone. For this reason, we've been investigating option 2 and have been testing Metabrainz's [troi recommendation](https://github.com/metabrainz/troi-recommendation-playground) system. This system meets the following criteria:

- It uses the same metadata corpus as Funkwhale (Musicbrainz)
- It is open source
- It uses compatible tooling (Python)

Troi isn't production-ready yet, but our testing shows that it is compatible with the content stored in Funkwhale. Our discussions with the Metabrainz team indicate they are interested in working with us to address our requirements.

## Feature behavior

The recommendation system should enable users to find related music based on filters/queries used in playlist creation and search. The following guiding principles should be followed:

1. The user should have the ability to enable and disable recommendations to suit their preferences
2. Funkwhale should only recommend related content that is accessible to the user

### Backend

The recommendation system should be reachable through an API that enables users/applications to exchange content information for a list of related content. The returned recommendations should adhere to the same structure as Funkwhale's existing content types.

#### Example related artists flow

```mermaid
sequenceDiagram
    User ->> API: Fetch artist
    API ->> Troi: Find related artists
    Troi -->> API: Return related artists as paginated artist list
    API -->> User: Return artist and related artists
```

#### Example playlist creation flow

```mermaid
sequenceDiagram
    User ->> API: Create playlist based on track list
    API ->> Troi: Fetch related recordings
    Troi -->> API: Return related content as paginated track list
    API -->> User: Return a generated playlist
```

The following also needs to be implemented:

1. The API should be configurable so that server admins can enable/disable recommendations at the server level

### Frontend

The user might experience recommendations in the following ways:

1. In search results
2. On object details pages (related artists, albums, tracks)
3. In the playlist builder as a tool to create recommendation-based playlists

The following also needs to be implemented:

1. The server admin *must* have the ability to enable/disable recommendations at the server level
2. If recommendations are enabled, users *must* have the ability to enable/disable recommendations for their account

## Availability

- [ ] Admin panel
- [x] App frontend
- [x] CLI

## Responsible parties

This project is dependent on the following Funkwhale working groups:

- Backend group – integrating the Troi library into the backend
- Design group – creating designs for how users and admins will interact with recommendations in the Funkwhale webapp
- Frontend group – implementing the designs created by the design group and defining the logic used by the app
- Documentation group:
    - Documenting the different workflows for users and admins
    - Documenting the recommendation logic used by Troi in Funkwhale

We are also dependent on the Metabrainz team to do the following:

- Distribute their library to Pypi so that we can integrate it
- Work with us to update their library to meet our requirements

## Open questions

1. What content do we want to recommend to users?
2. What performance impact will recommendations have on a large Funkwhale pod?
3. Should recommendation availability be split up on a feature-by-feature basis (e.g. an admin can enable related artists but disable playlist recommendations)?
4. How should we handle collections not tagged with MBIDs? Do we need to introduce automatic tagging before this feature can be implemented?

## Minimum viable product

The MVP for this project is a minimal implementation of recommendations *at the API level*. No frontend is required for the first step.

The MVP should implement [Troi's `world-trip` patch](https://github.com/metabrainz/troi-recommendation-playground/blob/main/troi/patches/world_trip.py) to create a playlist of content for a given continent. This should implement the following logic:

> When passed a **continent name** >
> **Troi** should return a list of recommended tracks from that continent to be inserted into a playlist

### Next steps

1. We need to discuss packaging and deployment with the Metabrainz team
2. We need to set out our end goal for recommendations in Funkwhale and work with Metabrainz to facilitate this
