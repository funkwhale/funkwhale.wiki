| Project | Stable/Main | Develop |
| ------ | ------ | ------- |
| Funkwhale | [![pipeline status](https://dev.funkwhale.audio/funkwhale/funkwhale/badges/stable/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/funkwhale/-/commits/stable) | [![pipeline status](https://dev.funkwhale.audio/funkwhale/funkwhale/badges/develop/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/funkwhale/-/commits/develop) |
| CLI | [![pipeline status](https://dev.funkwhale.audio/funkwhale/cli/badges/master/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/cli/-/commits/master) |
| Ansible | [![pipeline status](https://dev.funkwhale.audio/funkwhale/ansible/badges/master/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/ansible/-/commits/master) |
| CI | [![pipeline status](https://dev.funkwhale.audio/funkwhale/ci/badges/main/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/ci/-/commits/main) |
| Backend Test Image | [![pipeline status](https://dev.funkwhale.audio/funkwhale/backend-test-docker/badges/main/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/backend-test-docker/-/commits/main) |
| Blog | [![pipeline status](https://dev.funkwhale.audio/funkwhale/blog.funkwhale.audio/badges/main/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/blog.funkwhale.audio/-/commits/main) |
| Android |  [![pipeline status](https://dev.funkwhale.audio/funkwhale/funkwhale-android/badges/develop/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/funkwhale-android/-/commits/develop) |
| funkwhale.audio |  [![pipeline status](https://dev.funkwhale.audio/funkwhale/funkwhale.audio/badges/main/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/funkwhale.audio/-/commits/main) |
| Mopidy Plugin |  [![pipeline status](https://dev.funkwhale.audio/funkwhale/mopidy/badges/main/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/mopidy/-/commits/main) |
| VUI |  [![pipeline status](https://dev.funkwhale.audio/funkwhale/vui/badges/main/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/vui/-/commits/main) 
| api-client-python |  [![pipeline status](https://dev.funkwhale.audio/funkwhale/api-client-python/badges/main/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/api-client-python/-/commits/main) 
| api-client-typescript |  [![pipeline status](https://dev.funkwhale.audio/funkwhale/api-client-typescript/badges/main/pipeline.svg)](https://dev.funkwhale.audio/funkwhale/api-client-typescript/-/commits/main) 