Funkwhale has support for different APIs for different purposes. This document provides a rough overview about the APIs, their state of development and their usecase.

## Federation API

Used to exchange information between instances, implements ActivityPub and is able to talk to other implementations as well. This API is considered internal and may change over time, in alignment to the specification.

## Funkwhale Client API v1

Thats the major API clients use to talk to Funkwhale. The most used client is our built-in webinterface. The API is considered stable and won't change except bugfixes or fixing security issues. 

## Funkwhale Client API v2

Its a currently planned new version of the Client API with improvements. This API is highly work in progress and we suggest to use v1 until this changes or expect your client to break. 

## Subsonic API

Our implementation of the Subsonic API allows Subsonic Clients to consume music from Funkwhale. The API is not fully implemented, but most clients can be used. Extensions may be made over time.